declare module '*.html' {
  const value: string;
  export default value
}
declare module '*.scss' {
  const value: string;
  export default value
}

interface Window {
  loadModule: any
  anime: any
  detectModules: (el?: Element | Document) => void
  zes: any
  Cookies: any
  setLocale: any
  __webpack_public_path__: string
  containerSizes: {
    window: ZeContraints
    content: ZeContraints
    [key: string]: ZeContraints
  }
}

interface OverlayOptions {
  open?: boolean
  showClose?: boolean
  scrolling?: boolean
  width?: string
  height?: string
  template?: string
}


interface PositionData {
  left: number,
  top: number
  width: number | null
}

interface ZeValidatorParams {
  name: string // name of the input, or label, or placeholder
  value: boolean | string | number // the value of the input or if its checked
  meta?: string
}

interface ZeValidatorResponse {
  message?: string,
  valid: boolean
}

interface ZeValidators {
  [key: string]: (ZeValidatorParams) => ZeValidatorResponse
}

interface ZeRegisterValidator {
  key: string,
  handler: (ZeValidatorParams) => boolean
}
