interface MollyWatchOptions {
  root?: boolean
  eager?: boolean
}

interface MollyStore {
  addAction: (key: string, method: (value: any) => any) => void
  watch: (path: string, method: (storeValue: any) => void, options?: MollyWatchOptions) => void
  getValue: (path: string) => any
  set: (path: string, mutation: (storeValue: any) => any) => void
}

interface StoreModelElement extends Element {
  storeInitiated: boolean
  onchange: () => void
}
