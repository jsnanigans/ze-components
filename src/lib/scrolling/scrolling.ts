import store from '../store'

class Scrolling {
  public container: HTMLElement
  public scrollingDisabled: boolean
  public scrollPos: {x: number, y: number}

  public eventWrap: any

  constructor(container) {
    this.container = container
    this.scrollPos = {
      x: window.scrollX,
      y: window.scrollY,
    }
    this.scrollingDisabled = false

    this.container.addEventListener('scroll', (e) => this.handleScrollEvent(e), { passive: false })
    this.container.addEventListener('mousewheel', (e) => this.handleScrollEvent(e), { passive: false })
    this.container.addEventListener('wheel', (e) => this.handleScrollEvent(e), { passive: false })
    this.container.addEventListener('touchmove', (e) => this.handleScrollEvent(e), { passive: false })
  }

  public disableScrolling() {
    this.scrollPos = {
      x: window.scrollX,
      y: window.scrollY,
    }
    this.addListeners()
  }

  public enableScrolling() {
    this.removeListeners()
  }

  private addListeners() {
    this.scrollingDisabled = true
  }

  private removeListeners() {
    // this.container.removeEventListener('scroll', (e) => this.handleScrollEvent(e))
    // this.container.removeEventListener('mousewheel', (e) => this.handleScrollEvent(e))
    // this.container.removeEventListener('touchmove', (e) => this.handleScrollEvent(e))
    this.scrollingDisabled = false
  }

  private handleScrollEvent(event: any) {
    if (!this.scrollingDisabled) {
      return
    }

    let preventMove = true

    const scrollStyle = (event.target && event.target.style) ? event.target.style.overflow : ''

    if (scrollStyle.indexOf('auto') !== -1 || scrollStyle.indexOf('scroll') !== -1) {
      preventMove = false
    }

    if (preventMove) {
        this.preventDefault(event)
        this.container.scrollTo(this.scrollPos.x, this.scrollPos.y)
      }
  }

  private preventDefault(e: Event) {
    e = e || window.event
    if (e.preventDefault) {
        e.preventDefault()
    }
    e.returnValue = false
  }
}

const sc = new Scrolling(window)
store.set('main_scrolling', () => sc)
store.addAction('disable_scrolling', () => sc.disableScrolling())
store.addAction('enable_scrolling', () => sc.enableScrolling())
