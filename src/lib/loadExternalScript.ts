export default function loadScript(src: string) {
  return new Promise((resolve, reject) => {
    const r = false
    const s = document.createElement('script')
    const t = document.getElementsByTagName('script')[0]

    s.type = 'text/javascript'
    s.src = src
    s.onload = s.onreadystatechange = function() {
      if (!r && (!this.readyState || this.readyState === 'complete')) {
        resolve(this)
      } else {
        reject(this)
      }
    }
    t.parentNode.insertBefore(s, t)
  })
}
