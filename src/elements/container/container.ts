import { customElement, html, LitElement, property } from 'lit-element'

import styles from './container.scss'

window.containerSizes = {
  window: {
    height: '100vh',
  },
  content: {
    width: 'var(--content-width, 1000px)',
    maxWidth: '100%',
  },
}

@customElement('ze-container')
export default class Container extends LitElement {
  get sizeStyles() {
    return `
      width: ${this.sizeObject.width || 'auto'};
      height: ${this.sizeObject.height || 'auto'};
      min-width: ${this.sizeObject.minWidth || '0'};
      min-height: ${this.sizeObject.minHeight || '0'};
      max-width: ${this.sizeObject.maxWidth || 'none'};
      max-height: ${this.sizeObject.maxHeight || 'none'};
    `
  }

  public el: HTMLElement = this

  @property({ type: String, reflect: true }) public alignment:
    | 'center'
    | string = 'top-left'
  @property({ type: String, reflect: true }) public size: string = ''
  @property({ type: String }) private sizeObject: ZeContraints = {}
  @property({ type: String, reflect: true }) private constraints:
    | 'shrink'
    | 'grow' = 'shrink'

  public constructor() {
    super()
  }

  public firstUpdated() {
    super.firstUpdated()
    this.parseSizeOptions()
    this.requestUpdate()
  }

  public parseSizeOptions() {
    if (this.size) {
      if (typeof window.containerSizes[this.size] !== 'undefined') {
        this.sizeObject = window.containerSizes[this.size]
      }
    }

    // switch (this.size) {
    //   case 'window':
    //     this.sizeObject = {
    //       height: '100vh',
    //     }
    //     break
    // }
  }

  public render() {
    return html`
      <style>
        ${styles[0][1]}
        :host {
          ${this.sizeStyles}
        }
      </style>
      <section class="ze-container">
        <div class="ze-container__align">
          <slot></slot>
        </div>
      </section>
    `
  }

  public attributeChangedCallback(name, oldval, newval) {
    console.log('update', name)
    super.attributeChangedCallback(name, oldval, newval)
  }
}
