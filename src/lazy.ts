import 'lit-element'
import 'whatwg-fetch'

// used libraries
import anime from 'animejs'
window.anime = anime

// reset style
// import './reset.inline.scss'

// Core modules

window.loadModule = {
  'ze-container': () =>
    import(/* webpackChunkName: "container" */ './elements/container/container'),
}

const lazyDetect = () => {
  requestAnimationFrame(() => {
    requestAnimationFrame(() => {
      window.detectModules()
    })
  })
}

window.detectModules = (root = document) => {
  for (const key in window.loadModule) {
    if (key) {
      const query = root.querySelector(key)
      if (query) {
        window.loadModule[key]()
      }
    }
  }
}

let detectTo = 0
window.detectModules()
document.addEventListener('DOMContentLoaded', (event) => {
  window.detectModules()
  requestAnimationFrame(() => {
    requestAnimationFrame(() => {
      window.detectModules()
    })
  })
  observer.observe(document.body, config)
})

// mutation observer
const config = { attributes: false, childList: true, subtree: true }
const observer = new MutationObserver((mutationsList) => {
  for (const mutation of mutationsList) {
    if (mutation.type === 'childList') {
      clearTimeout(detectTo)
      detectTo = setTimeout(() => {
        requestAnimationFrame(() => {
          requestAnimationFrame(() => {
            window.detectModules()
          })
        })
      }, 30)
    }
  }
})

if (document.body) {
  observer.observe(document.body, config)
}
