const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin')

// const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const chalk = require('chalk')
const RuntimePublicPathPlugin = require('webpack-runtime-public-path-plugin')

const ie11 = ['ie 11']
const modernBrowsers = ['>=1%', 'not ie 11', 'not op_mini all']

const humanFileSize = (bytes, si) => {
  var thresh = si ? 1000 : 1024
  if (Math.abs(bytes) < thresh) {
    return bytes + ' B'
  }
  var units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
  var u = -1
  do {
    bytes /= thresh
    ++u
  } while (Math.abs(bytes) >= thresh && u < units.length - 1)
  return bytes.toFixed(1) + ' ' + units[u]
}

const build = (opt = {}) => {
  const port = 8898

  const config = {
    mode: opt.mode || 'production',
    entry: opt.module ? opt.module.path : {
      full: path.resolve(`src/full.ts`),
      lazy: path.resolve(`src/lazy.ts`),
      wcPolyfill: path.resolve(`src/webcomponent-polyfill.ts`),
      babelPolyfill: path.resolve(`src/babel-polyfill.ts`)
    },
    output: {
      path: path.resolve('dist', opt.browsers === modernBrowsers ? '' : 'es5', opt.module ? 'modules' : ''),
      filename: opt.module ? opt.module.filename : `[name].js`,
      chunkFilename: 'lib/[name].js',
      publicPath: opt.host ? opt.host : (opt.server ? '/' : '/dist/'),
      library: 'zeComponents'
      // libraryTarget: 'umd'
    },
    resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: ['.ts', '.tsx', '.js']
    },
    // externals: [],
    optimization: {
      // runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          // vendors: {
          //   test: /([\\/]node_modules[\\/]@polymer[\\/]polymer)/,
          //   name: 'polymer',
          //   enforce: true,
          //   chunks: 'all'
          // },
          // vendors: {
          //   test: /([\\/]node_modules[\\/]@polymer[\\/]lit-element)/,
          //   name: 'lit',
          //   enforce: true,
          //   chunks: 'all'
          // }
        }
      }
    },

    plugins: [
      // new HtmlWebpackPlugin({
      //   template: 'index.html'
      // })
      new RuntimePublicPathPlugin({
        runtimePublicPath: `window.zeComponentsPublicPath ? window.zeComponentsPublicPath + "${opt.es5 ? '/es5/' : '/'}" : "${opt.host ? opt.host : (opt.server ? '/' : '/dist/')}"`
      })
    ],

    module: {
      rules: [
        {
          test: /\.html$/,
          loader: 'html-loader'
        },
        {
          test: /\.inline\.scss$/,
          use: [{
            loader: 'style-loader'
          }, {
            loader: 'css-loader'
          }, {
            loader: 'postcss-loader'
          }, {
            loader: 'sass-loader',
            options: {
              includePaths: [
                path.resolve('node_modules')
              ]
            }
          }, {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                path.resolve(__dirname, '../src/theme/_variables.scss'),
                path.resolve(__dirname, '../src/theme/_custom.scss')
              ]
            }
          }]
        },
        {
          test: /(?<!inline)\.scss$/,
          use: [{
            loader: 'css-loader'
          }, {
            loader: 'postcss-loader'
          }, {
            loader: 'sass-loader',
            options: {
              includePaths: [
                path.resolve('node_modules')
              ]
            }
          }, {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                path.resolve(__dirname, '../src/theme/_variables.scss'),
                path.resolve(__dirname, '../src/theme/_custom.scss')
              ]
            }
          }]
        },
        {
          test: /\.ts$/,
          use: [{
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env',
                  { targets: { browsers: opt.browsers },
                    debug: false } ]
              ],
              plugins: [
                ['@babel/plugin-proposal-decorators', { 'legacy': true }],
                [ '@babel/plugin-syntax-object-rest-spread', { useBuiltIns: true } ],
                '@babel/plugin-syntax-dynamic-import',
                ['@babel/plugin-proposal-class-properties', { 'loose': true }]
              ]
            }
          }, {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
            }
          }]
        },
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env',
                  { targets: { browsers: opt.browsers },
                    debug: false } ]
              ],
              plugins: [
                ['@babel/plugin-proposal-decorators', { 'legacy': true }],
                [ '@babel/plugin-syntax-object-rest-spread', { useBuiltIns: true } ],
                '@babel/plugin-syntax-dynamic-import',
                ['@babel/plugin-proposal-class-properties', { 'loose': true }]
              ]
            }
          }
        }
      ]
    }
  }

  if (opt.server) {
    config.plugins = config.plugins.concat([
      new webpack.HotModuleReplacementPlugin({
        // multiStep: true
      }),
      new ErrorOverlayPlugin()
    ])

    config.entry = {
      // full: ['webpack-dev-server/client?http://localhost:' + port, path.resolve(`src/full.ts`)],
      lazy: ['webpack-dev-server/client?http://localhost:' + port, path.resolve(`src/lazy.ts`)]
    }

    config.devtool = 'cheap-module-eval-source-map'

    const devserver = new WebpackDevServer(webpack(config), {
      contentBase: path.resolve(''),
      // hot: true,
      // hotOnly: true,
      // compress: true,
      // historyApiFallback: true,
      // inline: true,
      // overlay: true,
      // https: true,

      stats: {
        colors: true,
        hash: false,
        version: false,
        timings: false,
        assets: false,
        chunks: false,
        modules: false,
        reasons: false,
        children: false,
        source: false,
        errors: true,
        errorDetails: false,
        warnings: true,
        publicPath: false
      },
      historyApiFallback: true,
      watchContentBase: true
    })

    devserver.listen(port, 'localhost', (err, result) => {
      if (err) {
        // return console.error(err)
        console.log(result.toString({
          chunks: false, // Makes the build much quieter
          colors: true // Shows colors in the console
        }))
      }

      console.log('listening on http://localhost:' + port)
    })
  } else {
    const v = opt.browsers === modernBrowsers ? 'ES6' : 'ES5'
    console.log('\n' + chalk.blue.bold(`Build started: `) + `for ` + chalk.cyan.bold(v))
    webpack(config,
      (err, stats) => {
        if (err || stats.hasErrors()) {
          // Handle errors here
          // console.log(stats)
          console.log(stats.toString({
            chunks: false, // Makes the build much quieter
            colors: true // Shows colors in the console
          }))
        }

        // console.log(stats)
        const path = 'dist' + stats.compilation.outputOptions.path.split('dist')[1]
        const assets = []
        for (const key in stats.compilation.assets) {
          assets.push({
            key,
            size: stats.compilation.assets[key]._value.length
          })
        }

        console.log(`
${chalk.cyan.bold(v)} finished:
${chalk.blue.bold(path)}
${assets.map((x, i) => {
    const size = humanFileSize(x.size)
    const max = x.key.indexOf('lib/') < 0 ? 244 : 80
    const name = x.size <= (max * 1024) ? chalk.green(x.key) : chalk.red(x.key)
    return `  ${i < assets.length - 1 ? `├` : '└'} ${name} ${chalk.yellow(size)}
`
  }).join('')}`)
        // console.log(stats.compilation.assets)
        // console.log(`Compiled: `)
      }
    )
  }
}

// build({
//   browsers: modernBrowsers,
// })

// build({
//   browsers: ie11,
//   name: 'ie11',
//   component: 'button',
// })
// build({
//   browsers: modernBrowsers,
//   name: 'other',
//   component: 'button',
// })

module.exports = {
  ie11,
  modernBrowsers,
  build
}
