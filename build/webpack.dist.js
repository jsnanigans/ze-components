// const fs = require('fs')
// const path = require('path')
const base = require('./webpack.base')

// https://components.view.agentur-ze.com/
const baseUrl = 'https://components.view.agentur-ze.com/'
const subDir = 'dev'

process.env.ASSET_PATH = `${baseUrl}/${subDir}`

base.build({
  browsers: base.modernBrowsers,
  host: `${baseUrl}/${subDir}/`
})
base.build({
  browsers: base.ie11,
  es5: true,
  host: `${baseUrl}/${subDir}/es5/`
})

// base.build({
//   browsers: base.modernBrowsers
// })
// base.build({
//   browsers: base.ie11
// })

// const modules = []

// const modulDirs = fs.readdirSync(path.resolve('components'))
//   .filter(x => {
//     if (x.endsWith('js')) return false
//     if (x === 'autoload') return false
//     return true
//   })

// for (const module of modulDirs) {
//   fs.readdirSync(path.resolve('components', module))
//     .filter(x => {
//       return x.startsWith('ze-') && x.endsWith('.js')
//     })
//     .forEach(x => {
//       modules.push([module, x])
//     })
// }

// for (const module of modules) {
//   base.build({
//     browsers: base.modernBrowsers,
//     module: {
//       name: module[0],
//       filename: module[1],
//       path: path.resolve('components', module[0], `${module[1]}`)
//     }
//   })
// }
