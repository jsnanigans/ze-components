const base = require('./webpack.base')

base.build({
  browsers: base.modernBrowsers,
  mode: 'development',
  server: true,
  host: 'http://localhost:8898/'
})
